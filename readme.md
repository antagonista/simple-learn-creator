# Simple Learn Creator

Program napisany w celu ułatwienia nauki pamięciowej poprzez powtarzanie. Przygotowany plik HTML (np. w Microsoft Word zapisany jak HTML; gdzie nagłówek jest pytaniem, a treść normalna pod pytaniem odpowiedzią) jest obrabiany przez skrypt Python'a, który tworzy plik HTML z zaimplementowanym kodem JavaScript tworzącym mechanizm nauki poprzez powtarzanie. 

Sam program działa w sposób prosty. Przerabiane są w cyklu trzy pytania, które nie osiągnęły odpowiedniej średniej. Każde pytanie posiada możliwość podejrzenia poprawnej odpowiedzi oraz subiektywnej oceny poprawności w skali od 1-5. Jeżeli jakieś zagadnienie jest znane lepiej (osiągnie wymaganą średnią naszych ocen) wypada z puli 3 powtarzanych pytań. Jeżeli wszystkie pytania osiągną odpowiednią średnią - na końcu jest wyświetlane podsumowanie wszystkich pytań wraz z osiągniętą treścią. 
