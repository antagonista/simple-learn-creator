from HTMLParser import HTMLParser
import os.path
import re

def DelNewLines (htmlText):
    #bozbycie sie znakow nowej lini z wyrazenia
    htmlText = htmlText.replace("\r","")
    htmlText = htmlText.replace("\n", "")
    htmlText = htmlText.replace("\r\n", "")
    return htmlText

def ContentSelect (data):
    wzor = '<div class=Word.+</div>'
    znalezione = re.search(wzor,data)
    return znalezione.group(0)

pytania = []
odpowiedzi = []

pytanie = False
odpowiedz = False
odpowiedzTemp = ""

# klasa obslugi parsera
class MyHTMLParser(HTMLParser):
    def handle_starttag(self, tag, attrs):

        global pytanie, odpowiedz, odpowiedzi, odpowiedzTemp

        # poczatek nowego pytania
        if (tag == 'h1'):
            pytanie = True
            if (odpowiedzTemp != ""):
                odpowiedzi.append(odpowiedzTemp)
                odpowiedzTemp == ""
        # poczatek odpowiedzi do pytania
        if (tag == 'p'):
            odpowiedz = True
            pytanie = False

    def handle_endtag(self, tag):

        if (tag == 'div'):
            odpowiedzi.append(odpowiedzTemp)
            odpowiedzTemp == ""

    def handle_data(self, data):

        global pytanie,pytania,odpowiedz,odpowiedzi,odpowiedzTemp

        if (pytanie == True):
            pytania.append(data)
            pytanie = False

        if (odpowiedz == True):
            odpowiedzTemp = odpowiedzTemp + '<p>' + data + '</p>'

def CreateTableQuestion(pytania,odpowiedzi):

    pytaniaTemp = '<table id="Pytania" border="0" style="display: none;">'

    for x in range(0,len(pytania)):
        pytaniaTemp = pytaniaTemp + '<tr>'
        pytaniaTemp = pytaniaTemp + '<td id="trescPytania' + str(x) + '">' + pytania[x] + '</td>'
        pytaniaTemp = pytaniaTemp + '<td id="odpowiedzPytania' + str(x) + '">' + odpowiedzi[x] + '</td>'
        pytaniaTemp = pytaniaTemp + '</tr>'

    pytaniaTemp = pytaniaTemp + '</table>'

    return pytaniaTemp

# PROGRAM

file = None

if (file == None):
    # pobieranie nazwy pliku do przerobienia oraz sprawdzenie czy istnieje
    while True:
        file = raw_input("Podaj nazwe pliku:")

        if (os.path.exists(file)):
            break
        else:
            print "nie znaleziono pliku.\n"

# plik istnieje -> otworzenie pliku
fileHandle = open(file,'r')

# wczytanie pliku do zmiennej
data = fileHandle.read()

# zamkniecie uchwytu
fileHandle.close()

# usuniecie znakow nowej lini
data = DelNewLines(data)

# wybranie samego contentu z pliku - <div wordclass cos tam
dataSelect = ContentSelect(data)

# parsowanie danych html - wyciagniecie danych
parser = MyHTMLParser()
parser.feed(dataSelect)

# tworzenie tabeli z pytaniami
tabela = CreateTableQuestion(pytania,odpowiedzi)

# otwarcie pliku szablonu
szablonHandle = open('data/szablon.html','r')

# pobranie szablonu
szablon = szablonHandle.read()

# zamkniecie uchwytu szablonu
szablonHandle.close()

# podstawienie danych w szablonie
ilePytan = len(pytania)
szablon = szablon.replace('[IloscPytan]',str(ilePytan))
szablon = szablon.replace('[PYTANIA]',tabela)

# zapis nowego pliku
nowaNazwaPliku = 'SL_'+file
newFile = open(str(nowaNazwaPliku),'w')
newFile.write(szablon)
newFile.close()

#konczenie pracy
print 'Utworzono plik o nazwie: '+nowaNazwaPliku
print 'end.'
raw_input('Nacisnij dowolny klawisz aby zakonczyc...')



